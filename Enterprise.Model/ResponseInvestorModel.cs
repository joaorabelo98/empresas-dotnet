﻿namespace Enterprise.Model
{
    public class ResponseInvestorModel
    {
        public InvestorModel investor { get; set; }
        public EnterpriseModel enterprise { get; set; }
        public bool success { get; set; }
    }
}
