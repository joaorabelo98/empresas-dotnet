﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Enterprise.Model
{
    public class InvestorModel
    {
        public int id { get; set; }
        public string investor_name { get; set; }
        public string email { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public double balance { get; set; }
        public string photo { get; set; }
        [NotMapped]
        public PortfolioModel portfolio { get; set; }
        public double portfolio_value { get; set; }
        public bool first_access { get; set; }
        public bool super_angel { get; set; }
    }
}
