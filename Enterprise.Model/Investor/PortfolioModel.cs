﻿using System.Collections.Generic;

namespace Enterprise.Model
{
    public class PortfolioModel
    {
        public int enterprises_number { get; set; }
        public IEnumerable<EnterpriseModel> enterprises { get; set; }
    }
}