﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Enterprise.Model
{
    public class EnterpriseModel
    {
        public int id { get; set; }
        public string email_enterprise { get; set; }
        public string facebook { get; set; }
        public string twitter { get; set; }
        public string linkedin { get; set; }
        public string phone { get; set; }
        public bool own_enterprise { get; set; }
        public string enterprise_name { get; set; }
        public string photo { get; set; }
        public string description { get; set; }
        public string country { get; set; }
        public double value { get; set; }
        public int shares { get; set; }
        public double share_price { get; set; }
        public int own_shares { get; set; }
        [NotMapped]
        public EnterpriseTypeModel enterprise_type { get; set; }
    }
}
