﻿namespace Enterprise.Model
{
    public class EnterpriseTypeModel
    {
        public int id { get; set; }
        public string enterprise_type_name { get; set; }
    }
}