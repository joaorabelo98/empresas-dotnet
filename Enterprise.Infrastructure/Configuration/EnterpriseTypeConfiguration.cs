﻿using Enterprise.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Enterprise.Infrastructure.Configuration
{
    class EnterpriseTypeConfiguration : IEntityTypeConfiguration<EnterpriseTypeModel>
    {
        public void Configure(EntityTypeBuilder<EnterpriseTypeModel> builder)
        {
            builder.HasKey(x => x.id);

            builder.Property(x => x.enterprise_type_name)
                .IsRequired()
                .HasMaxLength(120);
        }
    }
}
