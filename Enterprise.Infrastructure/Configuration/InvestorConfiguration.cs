﻿using Enterprise.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Enterprise.Infrastructure.Configuration
{
    public class InvestorConfiguration : IEntityTypeConfiguration<InvestorModel>
    {
        public void Configure(EntityTypeBuilder<InvestorModel> builder)
        {
            builder.HasKey(x => x.id);

            builder.Property(x => x.investor_name)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(x => x.email)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(x => x.country)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(x => x.balance)
                .IsRequired();

            builder.Property(x => x.photo);

            builder.Property(x => x.portfolio_value)
                .IsRequired();

            builder.Property(x => x.first_access)
                .IsRequired();

            builder.Property(x => x.super_angel)
                .IsRequired();
        }
    }
}
