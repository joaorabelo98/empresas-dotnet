﻿using Enterprise.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Enterprise.Infrastructure.Configuration
{
    public class EnterpriseConfiguration : IEntityTypeConfiguration<EnterpriseModel>
    {
        public void Configure(EntityTypeBuilder<EnterpriseModel> builder)
        {
            builder.HasKey(x => x.id);

            builder.Property(x => x.enterprise_name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.description)
                .IsRequired()
                .HasMaxLength(500);

            builder.Property(x => x.email_enterprise)
                .HasMaxLength(100);

            builder.Property(x => x.facebook)
                .HasMaxLength(100);

            builder.Property(x => x.twitter)
                .HasMaxLength(100);

            builder.Property(x => x.linkedin)
                .HasMaxLength(100);

            builder.Property(x => x.phone)
                .HasMaxLength(100);

            builder.Property(x => x.own_enterprise)
                .IsRequired();

            builder.Property(x => x.photo)
                .HasMaxLength(250);

            builder.Property(x => x.value)
                .IsRequired();

            builder.Property(x => x.shares)
                .IsRequired();

            builder.Property(x => x.share_price)
                .IsRequired();

            builder.Property(x => x.own_shares)
                .IsRequired();

            builder.Property(x => x.country)
                .IsRequired()
                .HasMaxLength(50);
            
            builder.Property(x => x.enterprise_type)
                .IsRequired();
        }
    }
}
