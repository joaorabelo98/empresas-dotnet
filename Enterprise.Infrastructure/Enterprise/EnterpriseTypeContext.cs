﻿using Enterprise.Infrastructure.Configuration;
using Enterprise.Model;
using Microsoft.EntityFrameworkCore;

namespace Enterprise.Infrastructure
{
    public class EnterpriseTypeContext : DbContext
    {
        public EnterpriseTypeContext(DbContextOptions<EnterpriseTypeContext> options) : base(options) { }

        public DbSet<EnterpriseTypeModel> EnterpriseType { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EnterpriseTypeConfiguration());
        }
    }
}

