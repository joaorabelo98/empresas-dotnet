﻿using Enterprise.Infrastructure.Configuration;
using Enterprise.Model;
using Microsoft.EntityFrameworkCore;

namespace Enterprise.Infrastructure
{
    public class EnterpriseContext : DbContext
    {
        public EnterpriseContext(DbContextOptions<EnterpriseContext> options) : base(options) { }

        public DbSet<EnterpriseModel> Enterprise { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EnterpriseConfiguration());
        }

    }
}
