﻿using Enterprise.Infrastructure.Configuration;
using Enterprise.Model;
using Microsoft.EntityFrameworkCore;

namespace Enterprise.Infrastructure.Investor
{
    public class InvestorContext : DbContext
    {
        public InvestorContext(DbContextOptions options) : base(options) { }

        public DbSet<InvestorModel> Investor { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new InvestorConfiguration());
        }
    }
}
