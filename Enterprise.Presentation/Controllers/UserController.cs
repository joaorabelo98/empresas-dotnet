﻿using Microsoft.AspNetCore.Mvc;

namespace Enterprise.Presentation.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpGet("auth/sign_in")]
        public string SignIn()
        {
            return "logado com sucesso";
        }
    }
}
