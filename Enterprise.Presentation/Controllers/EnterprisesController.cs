﻿using Microsoft.AspNetCore.Mvc;

namespace Enterprise.Presentation.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "Hello World";
        }
    }
}
